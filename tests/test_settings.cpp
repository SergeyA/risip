#include "settings_json_storage_provider.h"
#include "accounts_settings.h"
#include "codecs_settings.h"
#include "common_settings.h"
#include "transport_pofiles_settings.h"
#include "risip_settings.h"

// draft for test
void settings_test()
{

    SettingsJsonStorageProvider settingsStorageProvider("./settings.json");
    RisipSettings* settings = settingsStorageProvider.settings();

    AccountSettings accSet;
    TAccountUri idUri = "sip:tes1@192.168.3.147";
    accSet.m_transportProfileId = "t1";
    accSet.m_accountConfig.idUri = idUri;
    accSet.m_accountConfig.priority = 100;
    pj::AuthCredInfo credInfo = {"digest", "*", "user", 0, "passw"};
    accSet.m_accountConfig.sipConfig.authCreds.push_back(credInfo);
    accSet.m_accountConfig.callConfig.timerMinSESec = 1200;
    accSet.m_accountConfig.callConfig.timerSessExpiresSec = 22000;
    accSet.m_accountConfig.regConfig.registrarUri = "sip:192.168.3.147";
    accSet.m_accountConfig.mediaConfig.srtpUse = PJMEDIA_SRTP_DISABLED;
    settings->accountsSettings().addSettings(idUri, accSet);

    TransportPofileSettings transpSett;
    transpSett.m_profileId = "t1";
    transpSett.m_transportType = PJSIP_TRANSPORT_UDP;
    transpSett.m_transportConfig.port = 55555;
    settings->transportProfileSettings().addSettings(transpSett.m_profileId, transpSett);

    CodecSettings codecSettings;
    codecSettings.m_codecId = "speex/8000";
    codecSettings.m_codecParams.info.clockRate = 8000;
    codecSettings.m_codecParams.info.channelCnt = 55;
    codecSettings.m_codecParams.setting.frmPerPkt = 33;
    codecSettings.m_codecParams.setting.encFmtp.push_back(CodecFmtp() = {"QQQ", "AAA"});
    settings->codecsSettings().addSettings(codecSettings.m_codecId, codecSettings);

    CommonSettings &commonSettings = settings->commonSettings();
    commonSettings.m_defaultAccount = idUri;
    commonSettings.m_EndpointConfig.logConfig.level = 5;
    commonSettings.m_EndpointConfig.uaConfig.maxCalls = 4;
    commonSettings.m_EndpointConfig.medConfig.sndClockRate = 16000;

    settingsStorageProvider.write();



    SettingsJsonStorageProvider settingsStorageProvider_2("./settings.json");
    settingsStorageProvider_2.read();
    RisipSettings* settings_2 = settingsStorageProvider.settings();
}
