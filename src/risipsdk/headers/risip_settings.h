#ifndef RISIP_SETTINGS_H
#define RISIP_SETTINGS_H

#include "risipsdkglobal.h"

class CommonSettings;
class CodecsSettings;
class AccountsSettings;
class TransportPofilesSettings;

class RISIP_VOIPSDK_EXPORT RisipSettings
{
public:
    RisipSettings();
    virtual ~RisipSettings();

    const CommonSettings &commonSettings(void) const;
    CommonSettings &commonSettings(void);

    const CodecsSettings &codecsSettings(void) const;
    CodecsSettings &codecsSettings(void);

    const AccountsSettings &accountsSettings(void) const;
    AccountsSettings &accountsSettings(void);

    const TransportPofilesSettings &transportProfileSettings(void) const;
    TransportPofilesSettings &transportProfileSettings(void);

    void clear();

    virtual bool isModified(void) const { return m_isModified; }

protected:
    CommonSettings *m_pCommonSettings;
    CodecsSettings *m_pCodecsSettings;
    AccountsSettings *m_pAccountsSettings;
    TransportPofilesSettings *m_pTransportPofilesSettings;

    bool m_isModified = true;
};


#endif // RISIP_SETTINGS_H
