#ifndef RISIP_TYPES_H
#define RISIP_TYPES_H

#include <string>

enum Codecs {
    iLBC30 = 22,
    iLBC20,
    G711U,
    G711A,
    G722,
    G729,
    Speex20,
    Speex30,
    Opus
};

using TCodecId = std::string;

using TAccountUri = std::string;

using TTransportProfileId = std::string;


#endif // RISIP_TYPES_H
