#ifndef CODECS_SETTINGS_H
#define CODECS_SETTINGS_H

#include <string>
#include <unordered_map>
#include <pjsua2.hpp>

using namespace pj;

#include "risipsdkglobal.h"
#include "risip_types.h"



class RISIP_VOIPSDK_EXPORT CodecSettings
{

public:
    CodecSettings(){}
    virtual ~CodecSettings(){}

    TCodecId m_codecId; // see pjmedia_codec_info_to_id()
    uint8_t m_codecPriority;  // 0 - 255
    bool m_useDefaultParams;  // if true - m_codecParams is not used
    CodecParam m_codecParams; // pjsua structure with codecs params

    virtual bool isModified(void) const { return m_isModified; }

protected:
    bool m_isModified = true;
};
using TCodecSettingsMap = std::unordered_map<TCodecId,CodecSettings>;

using TCodecIdVector = std::vector<TCodecId>;

class RISIP_VOIPSDK_EXPORT CodecsSettings
{
public:
    CodecsSettings();
    virtual ~CodecsSettings();

    const TCodecIdVector& codecs() const;
    const CodecSettings *settings(const TCodecId &codecId) const;
    CodecSettings *settings(const TCodecId &codecId);
    void addSettings(const TCodecId &codecId, const CodecSettings &settings);
//    void addCodecSettings(const TCodecId &codecId, const CodecSettings &&settings);
    void removeSettings(const TCodecId &codecId);

    virtual bool isModified(void) const { return m_isModified; }

protected:
    TCodecSettingsMap m_codecSettingsMap;
    mutable TCodecIdVector m_codecs;
    mutable bool m_codecsListModified = true;
    bool m_isModified = true;
};

#endif // CODECS_SETTINGS_H
