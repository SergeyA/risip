#ifndef CONVERSION_HELPER_H
#define CONVERSION_HELPER_H

#include <string>
#include <QString>


class ConversionHelper
{
public:
    static std::string serverAddressFromRegistrarUri(const std::string &registrarUri);
    static std::string createSipUri(const std::string &username, const std::string &server);
    static std::string wrapToAngleBrackets(const std::string &str);
    static std::string createSipUriWithBrackets(const std::string &username, const std::string &server);
    static std::string usernameFromSipUri(const std::string &uri);

private:
    ConversionHelper() = default;
};



class ConversionHelperQ
{
public:
    static QString serverAddressFromRegistrarUri(const QString &registrarUri);
    static QString createSipUri(const QString &username, const QString &server);
    static QString wrapToAngleBrackets(const QString &str);
    static QString createSipUriWithBrackets(const QString &username, const QString &server);
    static QString usernameFromSipUri(const QString &uri);

private:
    ConversionHelperQ() = default;
};


#endif // CONVERSION_HELPER_H
