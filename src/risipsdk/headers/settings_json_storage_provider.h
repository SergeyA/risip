#ifndef SETTINGS_JSON_STORAGE_PROVIDER_H
#define SETTINGS_JSON_STORAGE_PROVIDER_H

#include <string>

#include "settings_provider.h"

class RisipSettings;

class SettingsJsonStorageProvider: public ISettingsProvider
{
public:
    SettingsJsonStorageProvider(const std::string& fileName);
    virtual ~SettingsJsonStorageProvider();

    RisipSettings* settings(void) const;

    virtual bool read();
    virtual bool write();

protected:
    std::string m_fileName;
    RisipSettings *m_pSettings = nullptr;

};


inline
RisipSettings* SettingsJsonStorageProvider::settings(void) const {
    return m_pSettings;
}


#endif // SETTINGS_JSON_STORAGE_PROVIDER_H
