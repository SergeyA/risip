#ifndef ACCOUNTS_SETTINGS_H
#define ACCOUNTS_SETTINGS_H


#include <string>
#include <unordered_map>
#include <vector>
#include <pjsua2.hpp>

using namespace pj;

#include "risipsdkglobal.h"
#include "risip_types.h"

class AccountSettings
{
public:
    AccountSettings(){}
    virtual ~AccountSettings(){}

    AccountConfig m_accountConfig; // pjsua structure with account config

    TTransportProfileId m_transportProfileId = "";
    bool m_autoSignIn = false;

    virtual bool isModified(void) const { return m_isModified; }

protected:
    bool m_isModified = true;
};
using TAccountSettingsMap = std::unordered_map<TAccountUri, AccountSettings>;

using TAccountUriVector = std::vector<TAccountUri>;

class AccountsSettings
{
public:
    AccountsSettings();
    virtual ~AccountsSettings();

    const TAccountUriVector& accounts() const;
    const AccountSettings *settings(const TAccountUri &uri) const;
    AccountSettings *settings(const TAccountUri &uri);
    void addSettings(const TAccountUri &uri, const AccountSettings &settings);
    void removeSettings(const TAccountUri &uri);

    virtual bool isModified(void) const { return m_isModified; }

protected:
    TAccountSettingsMap m_accountSettingsMap;
    mutable TAccountUriVector m_accounts;
    mutable bool m_accountsListModified = true;
    bool m_isModified = true;
};




#endif // ACCOUNTS_SETTINGS_H
