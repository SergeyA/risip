#ifndef SETTINGS_STORING_PROVIDER_H
#define SETTINGS_STORING_PROVIDER_H

class RisipSettings;

class ISettingsProvider
{
public:
    ISettingsProvider(){}
    virtual ~ISettingsProvider(){}

    virtual RisipSettings* settings(void) const = 0;

    virtual bool read() = 0;
    virtual bool write() = 0;

protected:

    RisipSettings* m_pSettings;

};


#endif // SETTINGS_STORING_PROVIDER_H
