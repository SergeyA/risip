/***********************************************************************************
**    Copyright (C) 2017  Petref Saraci
**
**    This program is free software: you can redistribute it and/or modify
**    it under the terms of the GNU General Public License as published by
**    the Free Software Foundation, either version 3 of the License, or
**    (at your option) any later version.
**
**    This program is distributed in the hope that it will be useful,
**    but WITHOUT ANY WARRANTY; without even the implied warranty of
**    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**    GNU General Public License for more details.
**
**    You have received a copy of the GNU General Public License
**    along with this program. See LICENSE.GPLv3
**    A copy of the license can be found also here <http://www.gnu.org/licenses/>.
**
************************************************************************************/

#ifndef RISIPACCOUNTLISTMODEL_H
#define RISIPACCOUNTLISTMODEL_H

#include "risipsdkglobal.h"
#include <QAbstractListModel>

namespace risip {

class RisipAccount;

class RISIP_VOIPSDK_EXPORT RisipAccountListModel: public QAbstractListModel
{
    Q_OBJECT
public:
    enum RisipAccountListDataRole {
        AccountURI = Qt::UserRole + 1,
        UserName,
        Password
    };

    Q_ENUM(RisipAccountListDataRole)
    Q_PROPERTY(QString activeAccountUri READ activeAccountUri WRITE setActiveAccountUri NOTIFY activeAccountUriChanged)
    Q_PROPERTY(risip::RisipAccount * activeAccount READ activeAccount)

    explicit RisipAccountListModel(QObject *parent = 0);
    ~RisipAccountListModel();

    QString activeAccountUri() const;
    void setActiveAccountUri(const QString &uri);

    risip::RisipAccount *activeAccount() const;

    QHash<int, QByteArray> roleNames() const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    RisipAccount *account(const QString &uri) const;
    RisipAccount *account(int index) const;
    bool exists(const QString &uri);
    bool clear();

Q_SIGNALS:
    void activeAccountUriChanged(const QString &uri);

private Q_SLOTS:
    void addSipAccount(RisipAccount *account);
    // TODO-S: consoder to remove this
    void removeSipAccount(RisipAccount *account);
    void removeSipAccount(const QString &uri);

private:
    friend class Risip;
    class Private;
    Private *m_data;
};

} //end of namespace

#endif // RISIPACCOUNTLISTMODEL_H
