/***********************************************************************************
**    Copyright (C) 2016  Petref Saraci
**
**    This program is free software: you can redistribute it and/or modify
**    it under the terms of the GNU General Public License as published by
**    the Free Software Foundation, either version 3 of the License, or
**    (at your option) any later version.
**
**    This program is distributed in the hope that it will be useful,
**    but WITHOUT ANY WARRANTY; without even the implied warranty of
**    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**    GNU General Public License for more details.
**
**    You have received a copy of the GNU General Public License
**    along with this program. See LICENSE.GPLv3
**    A copy of the license can be found also here <http://www.gnu.org/licenses/>.
**
************************************************************************************/

#ifndef RISIPACCOUNTCONFIGURATION_H
#define RISIPACCOUNTCONFIGURATION_H

#include <QObject>

#include <pjsua2.hpp>

#include "risipsdkglobal.h"
#include "accounts_settings.h"
#include "transport_pofiles_settings.h"

using namespace pj;


namespace risip {

class RisipAccount;

/**
 * @brief The RisipAccountConfiguration class
 *
 * provide editing of some parameters of account configuration. To connect instance of this class
 * with global storage of configuration you need to set account URI (in constructor or using 'uri' property).
 * Account configuration must be created previously. RisipAccountConfiguration doesn't create new account configuration.
 * To check if uri correspond to existent account use 'valid' property
 */
class RISIP_VOIPSDK_EXPORT RisipAccountConfiguration : public QObject
{
    Q_OBJECT
public:

    // TODO-SERGEY: move it configs related classes/files
//    enum Codecs {
//        iLBC30 = 22,
//        iLBC20,
//        G711U,
//        G711A,
//        G722,
//        G729,
//        Speex20,
//        Speex30,
//        Opus
//    };
//    Q_ENUM(Codecs)

    // TODO-S: after moving to Qt5.8 move to risip_types.h and declare with Q_ENUM_NS
    enum NetworkProtocol {
        UDP = 0,
        TCP = 1,
        TLS = 2
//        UDP6,
//        TCP6,
//        SCTP,
//        TLS6,
//        LOOP
    };
    Q_ENUM(NetworkProtocol)

    //pjsip-defined
    Q_PROPERTY(QString uri READ uri WRITE setUri NOTIFY uriChanged)
    Q_PROPERTY(QString userName READ userName WRITE setUserName NOTIFY userNameChanged)
    Q_PROPERTY(QString password READ password WRITE setPassword NOTIFY passwordChanged)
    Q_PROPERTY(QString scheme READ scheme WRITE setScheme NOTIFY schemeChanged)
    Q_PROPERTY(QString serverAddress READ serverAddress WRITE setServerAddress NOTIFY serverAddressChanged)
    Q_PROPERTY(QString proxyServer READ proxyServer WRITE setProxyServer NOTIFY proxyServerChanged)
//    Q_PROPERTY(int proxyPort READ proxyPort WRITE setProxyPort NOTIFY proxyPortChanged)
    Q_PROPERTY(int transportId READ transportId WRITE setTransportId NOTIFY transportIdChanged)
    Q_PROPERTY(bool encryptCalls READ encryptCalls WRITE setEncryptCalls NOTIFY encryptCallsChanged)
    Q_PROPERTY(bool valid READ valid NOTIFY validChanged)

    //user-defined
    Q_PROPERTY(int networkProtocol READ networkProtocol WRITE setNetworkProtocol NOTIFY networkProtocolChanged)
    Q_PROPERTY(int localPort READ localPort WRITE setLocalPort NOTIFY localPortChanged)
//    Q_PROPERTY(QList<int> availableCodecs READ availableCodecs WRITE setAvailableCodecs NOTIFY availableCodecsChanged)

    RisipAccountConfiguration(QObject *parent = 0, const QString &accountUri="");
    ~RisipAccountConfiguration();

    QString uri() const;
    void setUri(const QString &accountUri);

    QString userName() const;
    void setUserName(const QString &name);

    QString password() const;
    void setPassword(const QString &pass);

    QString scheme() const;
    void setScheme(const QString &credScheme);

    QString serverAddress();
    void setServerAddress(const QString &address);

    QString proxyServer() const;
    void setProxyServer(const QString &proxy);

    int proxyPort() const;
    void setProxyPort(int port);

    int transportId() const;
    void setTransportId(int transId);

    int networkProtocol() const;
    void setNetworkProtocol(int protocol);

    int localPort() const;
    void setLocalPort(int port);

    bool encryptCalls() const;
    void setEncryptCalls(bool encrypt);

    bool valid();

//    QList<int> availableCodecs() const;
//    void setAvailableCodecs(const QList<int> &codecs);


Q_SIGNALS:
    void uriChanged(const QString &uri);
    void userNameChanged(const QString &username);
    void passwordChanged(const QString &password);
    void schemeChanged(const QString &scheme);
    void serverAddressChanged(const QString &serverAddress);
    void proxyServerChanged(const QString &proxy);
    void proxyPortChanged(int &port);
    void transportIdChanged(int transportId);
    void networkProtocolChanged(int protocol);
    void localPortChanged(int port);
    void encryptCallsChanged(bool encrypt);
    void validChanged(bool valid);

private:
    AccountSettings* accountSettings() const;
    TransportPofileSettings *transportProfileSettings() const;

private:
    class Private;
    Private *m_data;
};

} //end of risip namespace

#endif // RISIPACCOUNTCONFIGURATION_H
