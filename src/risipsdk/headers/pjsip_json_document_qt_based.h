#ifndef PJSIP_QJSON_DOCUMENT_H
#define PJSIP_QJSON_DOCUMENT_H


#include <pjsua2/persistent.hpp>


class PjsipJsonDocumentQtBasedReaderOnly : public pj::PersistentDocument
{
public:
    PjsipJsonDocumentQtBasedReaderOnly();
    ~PjsipJsonDocumentQtBasedReaderOnly();
    virtual void loadFile(const std::string &filename) throw(pj::Error);
    virtual void loadString(const std::string &input) throw(pj::Error);
    virtual void saveFile(const std::string &filename) throw(pj::Error);
    virtual std::string saveString() throw(pj::Error);
    virtual pj::ContainerNode &getRootContainer() const;

    void addPointer(void* pval);
private:
    void initRoot();
    void clearPointers();

    class Private;
    Private *m_data;
};


#endif // PJSIP_QJSON_DOCUMENT_H
