#ifndef INSTANCES_FACTORY_H
#define INSTANCES_FACTORY_H


class ISettingsProvider;


class GlobalInstancesFactory
{
public:
    static ISettingsProvider &settingsProvider();

private:
    GlobalInstancesFactory();
    ~GlobalInstancesFactory();
};


#endif // INSTANCES_FACTORY_H
