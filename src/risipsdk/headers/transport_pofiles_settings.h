#ifndef TRANSPORT_POFILES_SETTINGS_H
#define TRANSPORT_POFILES_SETTINGS_H

#include <string>
#include <unordered_map>
#include <pjsua2.hpp>

using namespace pj;

#include "risipsdkglobal.h"
#include "risip_types.h"



class TransportPofileSettings
{
public:
    TransportPofileSettings() {}

    virtual ~TransportPofileSettings(){}

    TTransportProfileId m_profileId;
    pjsip_transport_type_e m_transportType;
    TransportConfig m_transportConfig; // pjsua structure with transport config

    virtual bool isModified(void) const { return m_isModified; }

protected:
    bool m_isModified = true;
};
using TransportPofileSettingsMap = std::unordered_map<TTransportProfileId, TransportPofileSettings>;

using TTransportProfileIdVector = std::vector<TTransportProfileId>;

class TransportPofilesSettings
{
public:
    TransportPofilesSettings();
    virtual ~TransportPofilesSettings();

    const TTransportProfileIdVector& transportProfiles() const;
    const TransportPofileSettings* settings(const TTransportProfileId& profileId) const;
    TransportPofileSettings* settings(const TTransportProfileId& profileId);
    void addSettings(const TTransportProfileId& profileId, const TransportPofileSettings& settings);
    void removeSettings(const TTransportProfileId& profileId);

    virtual bool isModified(void) const { return m_isModified; }

protected:
    TransportPofileSettingsMap m_transportPofileSettingsMap;
    mutable TTransportProfileIdVector m_transportProfiles;
    mutable bool m_transportProfilesListModified = true;
    bool m_isModified = true;
};

#endif // TRANSPORT_POFILES_SETTINGS_H
