#ifndef COMMON_SETTINGS_H
#define COMMON_SETTINGS_H


#include <string>
#include <pjsua2.hpp>

using namespace pj;

#include "risipsdkglobal.h"
#include "risip_types.h"

class RISIP_VOIPSDK_EXPORT CommonSettings
{

public:
    CommonSettings(){}
    virtual ~CommonSettings(){}

    TAccountUri m_defaultAccount; // url of default account
    EpConfig m_EndpointConfig;

    virtual bool isModified(void) const { return m_isModified; }

protected:
    bool m_isModified = true;
};



#endif // COMMON_SETTINGS_H
