/***********************************************************************************
**    Copyright (C) 2016  Petref Saraci
**
**    This program is free software: you can redistribute it and/or modify
**    it under the terms of the GNU General Public License as published by
**    the Free Software Foundation, either version 3 of the License, or
**    (at your option) any later version.
**
**    This program is distributed in the hope that it will be useful,
**    but WITHOUT ANY WARRANTY; without even the implied warranty of
**    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**    GNU General Public License for more details.
**
**    You have received a copy of the GNU General Public License
**    along with this program. See LICENSE.GPLv3
**    A copy of the license can be found also here <http://www.gnu.org/licenses/>.
**
************************************************************************************/

#include "risipaccountconfiguration.h"

#include "global_instances_factory.h"
#include "risip_settings.h"
#include "accounts_settings.h"
#include "transport_pofiles_settings.h"
#include "settings_provider.h"
#include "conversion_helper.h"


#include <QDebug>

namespace risip {

class RisipAccountConfiguration::Private
{
public:
    QString accountUri;

    RisipAccount *risipAccount;
    AccountConfig accountConfig;
    AuthCredInfo accountCredentials;
    TransportConfig transportConfiguration;
    int networkProtocol;
    QString proxyAddress;
    int proxyPort;
};

RisipAccountConfiguration::RisipAccountConfiguration(QObject *parent, const QString &accountUri)
    :QObject(parent)
    ,m_data(new Private)
{
    m_data->accountUri = accountUri;
}

RisipAccountConfiguration::~RisipAccountConfiguration()
{
    delete m_data;
    m_data = NULL;
}

QString RisipAccountConfiguration::uri() const
{
    return m_data->accountUri;

//    if(m_data->accountConfig.idUri.empty()) {
//        QString uri = QString("sip:") + userName() + QString("@") + serverAddress();
//        setUri(uri);
//        return uri;
//    }

//    return QString::fromStdString(m_data->accountConfig.idUri);
}

void RisipAccountConfiguration::setUri(const QString &accountUri)
{
    if (m_data->accountUri != accountUri) {
        m_data->accountUri = accountUri;
        emit uriChanged(m_data->accountUri);
    }
//    string accountUristr = accountUri.toStdString();
//    if(m_data->accountConfig.idUri != accountUristr) {
//        m_data->accountConfig.idUri = accountUristr;
//        emit uriChanged(accountUri);
//    }
}

QString RisipAccountConfiguration::userName() const
{
    auto settings = accountSettings();
    if (!settings) {
        return "";
    }
    // we suppose thet authCreds has at least one instance
    if (settings->m_accountConfig.sipConfig.authCreds.size() > 0) {
        return QString::fromStdString(settings->m_accountConfig.sipConfig.authCreds[0].username);
    }
    return "";
}

void RisipAccountConfiguration::setUserName(const QString &name)
{
    if(userName() != name) {
        auto settings = accountSettings();
        if (!settings) {
            return;
        }
        if (settings->m_accountConfig.sipConfig.authCreds.size() == 0) {
            pj::AuthCredInfo credInfo = {"digest", "*", name.toStdString(), 0, ""};
            settings->m_accountConfig.sipConfig.authCreds.push_back(credInfo);
        }
        else {
            settings->m_accountConfig.sipConfig.authCreds[0].username = name.toStdString();
        }
        emit userNameChanged(name);
    }
}

QString RisipAccountConfiguration::password() const
{
    auto settings = accountSettings();
    if (!settings) {
        return "";
    }
    // we suppose thet authCreds has at least one instance
    if (settings->m_accountConfig.sipConfig.authCreds.size() > 0) {
        return QString::fromStdString(settings->m_accountConfig.sipConfig.authCreds[0].data);
    }
    return "";
}

void RisipAccountConfiguration::setPassword(const QString &pass)
{
    if(password() != pass) {
        auto settings = accountSettings();
        if (!settings) {
            return;
        }
        if (settings->m_accountConfig.sipConfig.authCreds.size() == 0) {
            pj::AuthCredInfo credInfo = {"digest", "*", "", 0, pass.toStdString()};
            settings->m_accountConfig.sipConfig.authCreds.push_back(credInfo);
        }
        else {
            settings->m_accountConfig.sipConfig.authCreds[0].data = pass.toStdString();
            settings->m_accountConfig.sipConfig.authCreds[0].dataType = 0;
        }
        emit passwordChanged(pass);
    }
}

QString RisipAccountConfiguration::scheme() const
{
    auto settings = accountSettings();
    if (!settings) {
        return "";
    }
    // we suppose thet authCreds has at least one instance
    if (settings->m_accountConfig.sipConfig.authCreds.size() > 0) {
        return QString::fromStdString(settings->m_accountConfig.sipConfig.authCreds[0].scheme);
    }
    return "";
}

void RisipAccountConfiguration::setScheme(const QString &credScheme)
{
    if(scheme() != credScheme) {
        auto settings = accountSettings();
        if (!settings) {
            return;
        }
        if (settings->m_accountConfig.sipConfig.authCreds.size() == 0) {
            pj::AuthCredInfo credInfo = {credScheme.toStdString(), "*", "", 0, ""};
            settings->m_accountConfig.sipConfig.authCreds.push_back(credInfo);
        }
        else {
            settings->m_accountConfig.sipConfig.authCreds[0].scheme = credScheme.toStdString();
        }
        emit schemeChanged(credScheme);
    }
}

QString RisipAccountConfiguration::serverAddress()
{
    auto settings = accountSettings();
    if (!settings) {
        return "";
    }
    //server address always is stored as a "sip:serveraddress" format in pjsip, so removing "sip:"
    //comes in handy for passing the just the server address around
    if (!settings->m_accountConfig.regConfig.registrarUri.empty()) {
        return ConversionHelperQ::serverAddressFromRegistrarUri(
                    QString::fromStdString(settings->m_accountConfig.regConfig.registrarUri));
    }
    return "";
}

void RisipAccountConfiguration::setServerAddress(const QString &address)
{
    //always add the "sip:" prefix to properly store server address inside pjsip.
    if(serverAddress() != address) {
        auto settings = accountSettings();
        if (!settings) {
            return;
        }
        settings->m_accountConfig.regConfig.registrarUri = ConversionHelper::createSipUri("", address.toStdString());
        emit serverAddressChanged(address);
    }
}

/**
 * @brief RisipAccountConfiguration::proxyServer
 * @return
 *
 * at the moment return proxy server uri in the following format
 * "<sip:mydomain.org;transport=tcp>". In the future it can be only server name (address)
 */
QString RisipAccountConfiguration::proxyServer() const
{
    auto settings = accountSettings();
    if (!settings) {
        return "";
    }

    if (settings->m_accountConfig.sipConfig.proxies.size() > 0) {
        return QString::fromStdString(settings->m_accountConfig.sipConfig.proxies[0]);
    }
    else {
        return "";
    }
}

void RisipAccountConfiguration::setProxyServer(const QString &proxy)
{
    if(proxyServer() != proxy) {
        auto settings = accountSettings();
        if (!settings) {
            return;
        }

        if (settings->m_accountConfig.sipConfig.proxies.size() == 0) {
            settings->m_accountConfig.sipConfig.proxies.push_back(proxy.toStdString());
        }
        else {
            settings->m_accountConfig.sipConfig.proxies[0] = proxy.toStdString();
        }

        emit proxyServerChanged(proxy);
    }
}

//int RisipAccountConfiguration::proxyPort() const
//{
//    // TODO-S: implement if it will be needed
//}

//void RisipAccountConfiguration::setProxyPort(int port)
//{
//    // TODO-S: implement if it will be needed
//}

/**
 * @brief RisipAccountConfiguration::networkProtocol
 * @return transport protocol type (see pjsip_transport_type_e)
 *
 */
int RisipAccountConfiguration::networkProtocol() const
{
    auto settings = transportProfileSettings();
    if (!settings) {
        return PJSIP_TRANSPORT_UNSPECIFIED;
    }
    return settings->m_transportType;
}

void RisipAccountConfiguration::setNetworkProtocol(int protocol)
{
    if(networkProtocol() != protocol) {
        auto settings = transportProfileSettings();
        if (!settings) {
            return;
        }
        settings->m_transportType = static_cast<pjsip_transport_type_e>(protocol);
        emit networkProtocolChanged(protocol);
    }
}

int RisipAccountConfiguration::localPort() const
{
    auto settings = transportProfileSettings();
    if (!settings) {
        return 0;
    }
    return static_cast<int>(settings->m_transportConfig.port);
}

/**
 * @brief RisipAccountConfiguration::setLocalPort
 * @param port is local port that the engine will bind to for sending/accepting data
 *
 * Use this function to bind the sip client to a desired port
 *
 * Setting the port to 0 will simply enable the randomazing of local port property and the sip engine will
 * use any available port.
 */
void RisipAccountConfiguration::setLocalPort(int port)
{
    if(localPort() != port) {
        auto settings = transportProfileSettings();
        if (!settings) {
            return;
        }
        settings->m_transportConfig.port = port;
        emit localPortChanged(port);
    }
}

bool RisipAccountConfiguration::encryptCalls() const
{
    auto settings = accountSettings();
    if (!settings) {
        return "";
    }

    // TODO-S: check it. Why PJMEDIA_SRTP_OPTIONAL, not PJMEDIA_SRTP_MANDATORY ?
    if (settings->m_accountConfig.mediaConfig.srtpUse == PJMEDIA_SRTP_DISABLED) {
        return false;
    }
    else if (settings->m_accountConfig.mediaConfig.srtpUse == PJMEDIA_SRTP_OPTIONAL) {
        return true;
    }
    return false;
}

void RisipAccountConfiguration::setEncryptCalls(bool encrypt)
{
    if(encryptCalls() != encrypt) {
        auto settings = accountSettings();
        if (!settings) {
            return;
        }

        if(encrypt)
            // TODO-S: check it. Why PJMEDIA_SRTP_OPTIONAL, not PJMEDIA_SRTP_MANDATORY ?
            settings->m_accountConfig.mediaConfig.srtpUse = PJMEDIA_SRTP_OPTIONAL;
        else
            settings->m_accountConfig.mediaConfig.srtpUse = PJMEDIA_SRTP_DISABLED;

        emit encryptCallsChanged(encrypt);
    }
}

/**
 * @brief RisipAccountConfiguration::valid
 * @return true if uri correspond to existent account
 */
bool RisipAccountConfiguration::valid()
{
    return accountSettings() != nullptr;
}

AccountSettings *RisipAccountConfiguration::accountSettings() const
{
    return GlobalInstancesFactory::settingsProvider().settings()->accountsSettings().settings(uri().toStdString());
}

TransportPofileSettings *RisipAccountConfiguration::transportProfileSettings() const
{
    auto accSettings = accountSettings();
    if (!accSettings) {
        return nullptr;
    }
    return GlobalInstancesFactory::settingsProvider().settings()->transportProfileSettings().settings(accSettings->m_transportProfileId);
}

int RisipAccountConfiguration::transportId() const
{
    return m_data->accountConfig.sipConfig.transportId;
}

// TODO-S: remove this
void RisipAccountConfiguration::setTransportId(int transId)
{
    if(transportId() != transId) {
        m_data->accountConfig.sipConfig.transportId = transId;
        emit transportIdChanged(transId);
    }
}

} //end of risip namespace
