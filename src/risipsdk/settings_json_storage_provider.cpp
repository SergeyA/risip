#include "settings_json_storage_provider.h"

#include <qjsondocument.h>
#include <qjsonobject.h>
#include <qjsonarray.h>
#include <qjsonvalue.h>
#include <qfile.h>

#include "risip_settings.h"
#include "accounts_settings.h"
#include "transport_pofiles_settings.h"
#include "common_settings.h"
#include "codecs_settings.h"
#include "pjsip_json_document_qt_based.h"


class AccountSettingsRW
{
public:
    static bool read(QJsonValueRef val, AccountSettings &accountSettings);
    static bool write(QJsonObject& obj, const AccountSettings &accountSettings);
};

bool AccountSettingsRW::read(QJsonValueRef val, AccountSettings &accountSettings)
{
    if (!val.isObject()) {
        return false;
    }
    QJsonObject obj = val.toObject();

    accountSettings.m_transportProfileId = obj["transportProfileName"].toString().toStdString();
    accountSettings.m_autoSignIn = obj["autoSignIn"].toBool();

    // pj::AccountConfig is PersistentObject, so let use it
    QJsonDocument jdoc(obj);
    QString strJson(jdoc.toJson(QJsonDocument::Compact));
    std::string accountConfigStr = strJson.toUtf8().constData();
    try {
        PjsipJsonDocumentQtBasedReaderOnly doc;
        doc.loadString(accountConfigStr);
        pj::ContainerNode node = doc.getRootContainer();
        accountSettings.m_accountConfig.readObject(node);
    }
    catch (const pj::Error e) {
        // TODO-S:  handle error
        return false;
    }
    return true;
}

bool AccountSettingsRW::write(QJsonObject &val, const AccountSettings &accountSettings)
{
    val["transportProfileName"] = QJsonValue(accountSettings.m_transportProfileId.c_str());
    val["autoSignIn"] = QJsonValue(accountSettings.m_autoSignIn);

    // pj::AccountConfig is PersistentObject, so let use it
    pj::JsonDocument doc;
    pj::ContainerNode &node = doc.getRootContainer();
    accountSettings.m_accountConfig.writeObject(node);
    std::string accountConfigStr = doc.saveString();

    QJsonDocument jdoc = QJsonDocument::fromJson(accountConfigStr.c_str());
    val["AccountConfig"] = jdoc.object()["AccountConfig"];

    return true;
}


class AccountsSettingsRW
{
public:
    static bool read(QJsonValueRef val, AccountsSettings &accountsSettings);
    static bool write(QJsonArray& val, const AccountsSettings &accountsSettings);
};




bool AccountsSettingsRW::read(QJsonValueRef val, AccountsSettings &accountsSettings)
{
    if (!val.isArray()) {
        return false;
    }
    QJsonArray array = val.toArray();
    for (int i = 0; i < array.size(); ++i) {
        AccountSettings sett;
        if (!AccountSettingsRW::read(array[i], sett)){
            // TODO-S:  handle error
            return false;
        }
        accountsSettings.addSettings(sett.m_accountConfig.idUri, sett);
    }
    return true;
}

bool AccountsSettingsRW::write(QJsonArray& val, const AccountsSettings &accountsSettings)
{
    for(auto accountUrl: accountsSettings.accounts()) {
        QJsonObject obj;
        if (!AccountSettingsRW::write(obj, *accountsSettings.settings(accountUrl))) {
            // TODO-S:  handle error
            return false;
        }
        val.append(obj);
    }

    return true;
}

//////////////////////////////////////////////////////////

class TransportPofileSettingsRW
{
public:
    static bool read(const QJsonValue& val, TransportPofileSettings &transportPofileSettings);
    static bool write(QJsonObject& val, const TransportPofileSettings &transportPofileSettings);
};


bool TransportPofileSettingsRW::read(const QJsonValue &val, TransportPofileSettings &transportPofileSettings)
{
    if (!val.isObject()) {
        return false;
    }
    QJsonObject obj = val.toObject();

    transportPofileSettings.m_profileId = obj["profileId"].toString().toUtf8().constData();
    transportPofileSettings.m_transportType = pjsip_transport_type_e(obj["transportType"].toInt());

    // pj::TransportConfig is PersistentObject, so let use it
    QJsonDocument doc(obj);
    QString strJson(doc.toJson(QJsonDocument::Compact));
    std::string transportConfigStr = strJson.toUtf8().constData();
    try {
        PjsipJsonDocumentQtBasedReaderOnly doc;
        doc.loadString(transportConfigStr);
        pj::ContainerNode node = doc.getRootContainer();
        transportPofileSettings.m_transportConfig.readObject(node);
    }
    catch (const pj::Error e) {
        // TODO-S:  handle error
        return false;
    }
    return true;
}

bool TransportPofileSettingsRW::write(QJsonObject &val, const TransportPofileSettings &transportPofileSettings)
{
    val["profileId"] = QJsonValue(transportPofileSettings.m_profileId.c_str());
    val["transportType"] = QJsonValue(transportPofileSettings.m_transportType);

    // pj::TransportConfig is PersistentObject, so let use it
    pj::JsonDocument doc;
    pj::ContainerNode &node = doc.getRootContainer();
    transportPofileSettings.m_transportConfig.writeObject(node);
    std::string configStr = doc.saveString();

    QJsonDocument jdoc = QJsonDocument::fromJson(configStr.c_str());
    val["TransportConfig"] = jdoc.object()["TransportConfig"];

    return true;
}



class TransportPofilesSettingsRW
{
public:
    static bool read(const QJsonValue& val, TransportPofilesSettings &transportPofilesSettings);
    static bool write(QJsonArray& val, const TransportPofilesSettings &transportPofilesSettings);
};



bool TransportPofilesSettingsRW::read(const QJsonValue &val, TransportPofilesSettings &transportPofilesSettings)
{
    if (!val.isArray()) {
        return false;
    }
    QJsonArray array = val.toArray();
    for (int i = 0; i < array.size(); ++i) {
        TransportPofileSettings sett;
        if (!TransportPofileSettingsRW::read(array[i], sett)){
            // TODO-S:  handle error
            return false;
        }
        transportPofilesSettings.addSettings(sett.m_profileId, sett);
    }
    return true;
}

bool TransportPofilesSettingsRW::write(QJsonArray &val, const TransportPofilesSettings &transportPofilesSettings)
{
    for(auto transportProfileId: transportPofilesSettings.transportProfiles()) {
        QJsonObject obj;
        if (!TransportPofileSettingsRW::write(obj, *transportPofilesSettings.settings(transportProfileId))) {
            // TODO-S:  handle error
            return false;
        }
        val.append(obj);
    }

    return true;
}

///////////////////////////////////////////////////////////

class CommonSettingsRW
{
public:
    static bool read(QJsonValueRef val, CommonSettings &settings);
    static bool write(QJsonObject& obj, const CommonSettings &settings);
};


bool CommonSettingsRW::read(QJsonValueRef val, CommonSettings &settings)
{
    if (!val.isObject()) {
        return false;
    }
    QJsonObject obj = val.toObject();

    settings.m_defaultAccount = obj["defaultAccount"].toString().toStdString();

    // pj::EpConfig is PersistentObject, so let use it
    QJsonDocument jdoc(obj);
    QString strJson(jdoc.toJson(QJsonDocument::Compact));
    std::string configStr = strJson.toUtf8().constData();
    try {
        PjsipJsonDocumentQtBasedReaderOnly doc;
        doc.loadString(configStr);
        pj::ContainerNode node = doc.getRootContainer();
        settings.m_EndpointConfig.readObject(node);
    }
    catch (const pj::Error e) {
        // TODO-S:  handle error
        return false;
    }

    return true;
}

bool CommonSettingsRW::write(QJsonObject &obj, const CommonSettings &settings)
{
    obj["defaultAccount"] = QJsonValue(settings.m_defaultAccount.c_str());

    // pj::EpConfig is PersistentObject, so let use it
    pj::JsonDocument doc;
    pj::ContainerNode &node = doc.getRootContainer();
    settings.m_EndpointConfig.writeObject(node);
    std::string configStr = doc.saveString();

    QJsonDocument jdoc = QJsonDocument::fromJson(configStr.c_str());
    obj["EpConfig"] = jdoc.object()["EpConfig"];

    return true;
}


///////////////////////////////////////////////////////////


class CodecSettingsRW
{
public:
    static bool read(QJsonValueRef val, CodecSettings &settings);
    static bool write(QJsonObject& obj, const CodecSettings &settings);

private:
    static bool readCodecFmtpVector(QJsonValueRef val, pj::CodecFmtpVector &fmtpVector);
    static bool writeCodecFmtpVector(QJsonArray& val, const pj::CodecFmtpVector &fmtpVector);
};

bool CodecSettingsRW::read(QJsonValueRef val, CodecSettings &settings)
{
    if (!val.isObject()) {
        return false;
    }
    QJsonObject obj = val.toObject();

    settings.m_codecId = obj["codecId"].toString().toStdString();
    settings.m_codecPriority = obj["codecPriority"].toInt();
    settings.m_useDefaultParams = obj["useDefaultParams"].toBool();

    if (!settings.m_useDefaultParams) {
        QJsonObject codecParam = obj["CodecParam"].toObject();
        QJsonObject codecParamInfo = codecParam["CodecParamInfo"].toObject();
        QJsonObject codecParamSetting = codecParam["CodecParamSetting"].toObject();

        settings.m_codecParams.info.clockRate = codecParamInfo["clockRate"].toInt();
        settings.m_codecParams.info.channelCnt = codecParamInfo["channelCnt"].toInt();
        settings.m_codecParams.info.avgBps = codecParamInfo["avgBps"].toInt();
        settings.m_codecParams.info.maxBps = codecParamInfo["maxBps"].toInt();
        settings.m_codecParams.info.maxRxFrameSize = codecParamInfo["maxRxFrameSize"].toInt();
        settings.m_codecParams.info.frameLen = codecParamInfo["frameLen"].toInt();
        settings.m_codecParams.info.pcmBitsPerSample = codecParamInfo["pcmBitsPerSample"].toInt();
        settings.m_codecParams.info.pt = codecParamInfo["pt"].toInt();
        settings.m_codecParams.info.fmtId = static_cast<pjmedia_format_id>(codecParamInfo["fmtId"].toInt());

        settings.m_codecParams.setting.frmPerPkt = codecParamSetting["frmPerPkt"].toInt();
        settings.m_codecParams.setting.vad = codecParamSetting["vad"].toBool();
        settings.m_codecParams.setting.cng = codecParamSetting["cng"].toBool();
        settings.m_codecParams.setting.penh = codecParamSetting["penh"].toBool();
        settings.m_codecParams.setting.plc = codecParamSetting["plc"].toBool();
        settings.m_codecParams.setting.reserved = codecParamSetting["reserved"].toBool();
        readCodecFmtpVector(codecParamSetting["encFmtp"], settings.m_codecParams.setting.encFmtp);
        readCodecFmtpVector(codecParamSetting["decFmtp"], settings.m_codecParams.setting.decFmtp);
    }
    return true;
}

bool CodecSettingsRW::write(QJsonObject &val, const CodecSettings &settings)
{
    val["codecId"] = QJsonValue(settings.m_codecId.c_str());
    val["codecPriority"] = QJsonValue(settings.m_codecPriority);
    val["useDefaultParams"] = QJsonValue(settings.m_useDefaultParams);

    if (!settings.m_useDefaultParams) {
        QJsonObject codecParam;
        QJsonObject codecParamInfo;
        QJsonObject codecParamSetting;

        // TODO-S: possible a unspecified result (unsigned int -> int)
        codecParamInfo["clockRate"] = static_cast<int>(settings.m_codecParams.info.clockRate);
        codecParamInfo["channelCnt"] = static_cast<int>(settings.m_codecParams.info.channelCnt);
        codecParamInfo["avgBps"] = static_cast<int>(settings.m_codecParams.info.avgBps);
        codecParamInfo["maxBps"] = static_cast<int>(settings.m_codecParams.info.maxBps);
        codecParamInfo["maxRxFrameSize"] = static_cast<int>(settings.m_codecParams.info.maxRxFrameSize);
        codecParamInfo["frameLen"] = static_cast<int>(settings.m_codecParams.info.frameLen);
        codecParamInfo["pcmBitsPerSample"] = static_cast<int>(settings.m_codecParams.info.pcmBitsPerSample);
        codecParamInfo["pt"] = static_cast<int>(settings.m_codecParams.info.pt);
        codecParamInfo["fmtId"] = static_cast<int>(settings.m_codecParams.info.fmtId);

        codecParamSetting["frmPerPkt"] = static_cast<int>(settings.m_codecParams.setting.frmPerPkt);
        codecParamSetting["vad"] = settings.m_codecParams.setting.vad;
        codecParamSetting["cng"] = settings.m_codecParams.setting.cng;
        codecParamSetting["penh"] = settings.m_codecParams.setting.penh;
        codecParamSetting["plc"] = settings.m_codecParams.setting.plc;
        codecParamSetting["reserved"] = settings.m_codecParams.setting.reserved;
        QJsonArray encFmtp;
        writeCodecFmtpVector(encFmtp, settings.m_codecParams.setting.encFmtp);
        codecParamSetting["encFmtp"] = encFmtp;
        QJsonArray decFmtp;
        writeCodecFmtpVector(decFmtp, settings.m_codecParams.setting.decFmtp);
        codecParamSetting["decFmtp"] = decFmtp;

        codecParam["CodecParamInfo"] = codecParamInfo;
        codecParam["CodecParamSetting"] = codecParamSetting;
        val["CodecParam"] = codecParam;
    }
    return true;
}

bool CodecSettingsRW::readCodecFmtpVector(QJsonValueRef val, pj::CodecFmtpVector &fmtpVector)
{
    if (!val.isArray()) {
        return false;
    }
    QJsonArray array = val.toArray();
    for (int i = 0; i < array.size(); ++i) {
        CodecFmtp fmtp;
        QJsonObject obj = array[i].toObject();
        fmtp.name = obj["name"].toString().toStdString();
        fmtp.val = obj["val"].toString().toStdString();
        fmtpVector.push_back(fmtp);
    }
    return true;
}

bool CodecSettingsRW::writeCodecFmtpVector(QJsonArray& val, const pj::CodecFmtpVector &fmtpVector)
{
    for(auto fmtp: fmtpVector) {
        QJsonObject obj;
        obj["name"] = QString::fromStdString(fmtp.name);
        obj["val"] = QString::fromStdString(fmtp.val);
        val.append(obj);
    }

    return true;
}




class CodecsSettingsRW
{
public:
    static bool read(QJsonValueRef val, CodecsSettings &settings);
    static bool write(QJsonArray& val, const CodecsSettings &settings);
};

bool CodecsSettingsRW::read(QJsonValueRef val, CodecsSettings &settings)
{
    if (!val.isArray()) {
        return false;
    }
    QJsonArray array = val.toArray();
    for (int i = 0; i < array.size(); ++i) {
        CodecSettings sett;
        if (!CodecSettingsRW::read(array[i], sett)){
            // TODO-S:  handle error
            return false;
        }
        settings.addSettings(sett.m_codecId, sett);
    }
    return true;
}

bool CodecsSettingsRW::write(QJsonArray& val, const CodecsSettings &settings)
{
    for(auto codecId: settings.codecs()) {
        QJsonObject obj;
        if (!CodecSettingsRW::write(obj, *settings.settings(codecId))) {
            // TODO-S:  handle error
            return false;
        }
        val.append(obj);
    }

    return true;
}

//////////////////////////////////////////////////////////

///
/// \brief SettingsJsonStorageProvider::SettingsJsonStorageProvider
/// \param fileName
///
SettingsJsonStorageProvider::SettingsJsonStorageProvider(const std::string &fileName):
    m_fileName(fileName),
    m_pSettings(new RisipSettings())
{

}

SettingsJsonStorageProvider::~SettingsJsonStorageProvider()
{
    delete m_pSettings;
}


bool SettingsJsonStorageProvider::read()
{
    if (m_pSettings == nullptr)
    {
        // TODO-S: how to handle error? throw exception?
        return false;
    }

    QFile loadFile(m_fileName.c_str());
    if (!loadFile.open(QIODevice::ReadOnly)) {
        qWarning("Couldn't open settings file.");
        return false;
    }

    QByteArray saveData = loadFile.readAll();
    QJsonDocument loadDoc(QJsonDocument::fromJson(saveData));
    QJsonObject sett = loadDoc.object();


    if (!sett.contains("CommonSettings")) {
        return false;
    }
    if (!CommonSettingsRW::read(sett["CommonSettings"], m_pSettings->commonSettings())) {
        return false;
    }


    if (!sett.contains("AccountsSettings")) {
        return false;
    }
    if (!AccountsSettingsRW::read(sett["AccountsSettings"], m_pSettings->accountsSettings())) {
        return false;
    }


    if (!sett.contains("TransportPofilesSettings")) {
        return false;
    }
    if (!TransportPofilesSettingsRW::read(sett["TransportPofilesSettings"], m_pSettings->transportProfileSettings())) {
        return false;
    }


    if (!sett.contains("CodecsSettings")) {
        return false;
    }
    if (!CodecsSettingsRW::read(sett["CodecsSettings"], m_pSettings->codecsSettings())) {
        return false;
    }

    return true;
}

bool SettingsJsonStorageProvider::write()
{
    if (m_pSettings == nullptr)
    {
        // TODO-S: how to handle error? throw exception?
        return false;
    }

    QFile saveFile(m_fileName.c_str());

    if (!saveFile.open(QIODevice::WriteOnly)) {
        qWarning("Couldn't open settings file.");
        return false;
    }

    QJsonObject saveObject;

    QJsonObject commonSettings;
    if (!CommonSettingsRW::write(commonSettings, m_pSettings->commonSettings())) {
        // TODO-S: how to handle error? throw exception?
        return false;
    }
    saveObject["CommonSettings"] = commonSettings;

    QJsonArray accountsSettings;
    if (!AccountsSettingsRW::write(accountsSettings, m_pSettings->accountsSettings())) {
        // TODO-S: how to handle error? throw exception?
        return false;
    }
    saveObject["AccountsSettings"] = accountsSettings;

    QJsonArray transportPofilesSettings;
    if (!TransportPofilesSettingsRW::write(transportPofilesSettings, m_pSettings->transportProfileSettings())) {
        // TODO-S: how to handle error? throw exception?
        return false;
    }
    saveObject["TransportPofilesSettings"] = transportPofilesSettings;

    QJsonArray codecsSettings;
    if (!CodecsSettingsRW::write(codecsSettings, m_pSettings->codecsSettings())) {
        // TODO-S: how to handle error? throw exception?
        return false;
    }
    saveObject["CodecsSettings"] = codecsSettings;

    QJsonDocument saveDoc(saveObject);
    saveFile.write(saveDoc.toJson());

    return true;
}

