#include "conversion_helper.h"

#include <string>


std::string ConversionHelper::serverAddressFromRegistrarUri(const std::string &registrarUri)
{
    //server address always is stored as a "sip:serveraddress" format in pjsip, so removing "sip:"
    //comes in handy for passing the just the server address around
    if(!registrarUri.empty()) {
        std::string removedStr = "sip:";
        size_t pos = registrarUri.find(removedStr);
        if (pos != std::string::npos)
        {
            std::string result = registrarUri;
            result.erase(pos, removedStr.length());
            return result;
        }
    }
    return "";
}

std::string ConversionHelper::createSipUri(const std::string &username, const std::string &server)
{
    if (!username.empty()) {
        std::string uri = std::string("sip:")
                +username
                +std::string("@")
                +server;

        return uri;
    }
    else {
        std::string uri = std::string("sip:")
                +server;
        return uri;

    }
}


std::string ConversionHelper::wrapToAngleBrackets(const std::string &str)
{
    return std::string("<") + str + ">";
}


std::string ConversionHelper::createSipUriWithBrackets(const std::string &username, const std::string &server)
{
    //    RFC 3261 section 20: "The Contact, From, and To header fields contain a
    //    URI.  If the URI contains a comma, question mark or semicolon, the URI
    //    MUST be enclosed in angle brackets (< and >).  Any URI parameters are
    //    contained within these brackets.  If the URI is not enclosed in angle
    //    brackets, any semicolon-delimited parameters are header-parameters, not
    //    URI parameters."
    return wrapToAngleBrackets(createSipUri(username, server));
}

std::string ConversionHelper::usernameFromSipUri(const std::string &uri)
{
    // TODO-S: may be is better to use regexp instead
    std::string removedStr = "sip:";
    std::string::size_type firstPos = uri.find(removedStr);
    if (firstPos == std::string::npos)
    {
        return std::string("");
    }
    firstPos += removedStr.length();
    std::string::size_type lastPos = uri.find("@");
    if ((firstPos < lastPos) && (lastPos < uri.length())) {
        std::string username = uri.substr(firstPos, lastPos - firstPos);
        return username;
    }
    return std::string("");

}


////////////////////////////////////////////////////////////////


QString ConversionHelperQ::serverAddressFromRegistrarUri(const QString &registrarUri)
{
    //server address always is stored as a "sip:serveraddress" format in pjsip, so removing "sip:"
    //comes in handy for passing the just the server address around
    if(!registrarUri.isEmpty()) {
        QString result = registrarUri;
        return result.remove("sip:");
    }
    return "";
}

QString ConversionHelperQ::createSipUri(const QString &username, const QString &server)
{
    if (!username.isEmpty()) {
        QString uri = QString("sip:")
                +username
                +QString("@")
                +server;
        return uri;
    }
    else {
        QString uri = QString("sip:")
                +server;
        return uri;

    }
}

QString ConversionHelperQ::wrapToAngleBrackets(const QString &str)
{
    return QString("<") + str + ">";
}

QString ConversionHelperQ::createSipUriWithBrackets(const QString &username, const QString &server)
{
    //    RFC 3261 section 20: "The Contact, From, and To header fields contain a
    //    URI.  If the URI contains a comma, question mark or semicolon, the URI
    //    MUST be enclosed in angle brackets (< and >).  Any URI parameters are
    //    contained within these brackets.  If the URI is not enclosed in angle
    //    brackets, any semicolon-delimited parameters are header-parameters, not
    //    URI parameters."
    return wrapToAngleBrackets(createSipUri(username, server));
}

QString ConversionHelperQ::usernameFromSipUri(const QString &uri)
{
    // TODO-S: may be is better to use regexp instead
    QString removedStr = "sip:";
    int firstPos = uri.indexOf(removedStr);
    if (firstPos == -1)
    {
        return QString("");
    }
    firstPos += removedStr.length();
    int lastPos = uri.indexOf("@");
    if ((firstPos < lastPos) && (lastPos < uri.length())) {
        QString username = uri.mid(firstPos, lastPos - firstPos);
        return username;
    }
    return QString("");
}
