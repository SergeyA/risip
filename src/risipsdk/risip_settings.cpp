#include "risip_settings.h"

#include "accounts_settings.h"
#include "codecs_settings.h"
#include "common_settings.h"
#include "transport_pofiles_settings.h"

RisipSettings::RisipSettings():
    m_pCommonSettings(new CommonSettings()),
    m_pCodecsSettings(new CodecsSettings()),
    m_pAccountsSettings(new AccountsSettings()),
    m_pTransportPofilesSettings(new TransportPofilesSettings())
{
}

RisipSettings::~RisipSettings()
{
    delete m_pTransportPofilesSettings;
    delete m_pAccountsSettings;
    delete m_pCodecsSettings;
    delete m_pCommonSettings;
}

const CommonSettings &RisipSettings::commonSettings() const
{
    return *m_pCommonSettings;
}

CommonSettings &RisipSettings::commonSettings()
{
    return *m_pCommonSettings;
}

const CodecsSettings &RisipSettings::codecsSettings() const
{
    return *m_pCodecsSettings;
}

CodecsSettings &RisipSettings::codecsSettings()
{
    return *m_pCodecsSettings;
}

const AccountsSettings &RisipSettings::accountsSettings() const
{
    return *m_pAccountsSettings;
}

AccountsSettings &RisipSettings::accountsSettings()
{
    return *m_pAccountsSettings;
}

const TransportPofilesSettings &RisipSettings::transportProfileSettings() const
{
    return *m_pTransportPofilesSettings;
}

TransportPofilesSettings &RisipSettings::transportProfileSettings()
{
    return *m_pTransportPofilesSettings;
}

void RisipSettings::clear()
{
    // TODO-S: implement this
    // assert(0);
}
