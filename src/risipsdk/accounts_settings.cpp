#include "accounts_settings.h"

AccountsSettings::AccountsSettings()
{
}

AccountsSettings::~AccountsSettings()
{
}

const TAccountUriVector &AccountsSettings::accounts() const
{
    if (m_accountsListModified) {
        m_accounts.clear();
        m_accounts.reserve(m_accountSettingsMap.size());
        for(auto kv : m_accountSettingsMap) {
            m_accounts.push_back(kv.first);
        }
        m_accountsListModified = false;
    }
    return m_accounts;
}

const AccountSettings *AccountsSettings::settings(const TAccountUri &uri) const
{
    if (m_accountSettingsMap.find(uri) != m_accountSettingsMap.end())
    {
        return &m_accountSettingsMap.at(uri);
    }
    return nullptr;
}

AccountSettings *AccountsSettings::settings(const TAccountUri &uri)
{
    if (m_accountSettingsMap.find(uri) != m_accountSettingsMap.end())
    {
        return &m_accountSettingsMap.at(uri);
    }
    return nullptr;
}

void AccountsSettings::addSettings(const TAccountUri &uri, const AccountSettings &settings)
{
    m_accountSettingsMap[uri] = settings;
    m_accountsListModified = true;
}

void AccountsSettings::removeSettings(const TAccountUri &uri)
{
    if (m_accountSettingsMap.find(uri) != m_accountSettingsMap.end())
    {
        m_accountSettingsMap.erase(uri);
        m_accountsListModified = true;
    }
}
