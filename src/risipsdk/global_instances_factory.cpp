#include "global_instances_factory.h"

#include "settings_json_storage_provider.h"


ISettingsProvider &GlobalInstancesFactory::settingsProvider()
{
    static SettingsJsonStorageProvider instance("./settings.json");
    return instance;
}
