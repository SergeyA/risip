/***********************************************************************************
**    Copyright (C) 2016  Petref Saraci
**    http://risip.io
**
**    This program is free software: you can redistribute it and/or modify
**    it under the terms of the GNU General Public License as published by
**    the Free Software Foundation, either version 3 of the License, or
**    (at your option) any later version.
**
**    This program is distributed in the hope that it will be useful,
**    but WITHOUT ANY WARRANTY; without even the implied warranty of
**    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**    GNU General Public License for more details.
**
**    You have received a copy of the GNU General Public License
**    along with this program. See LICENSE.GPLv3
**    A copy of the license can be found also here <http://www.gnu.org/licenses/>.
**
************************************************************************************/

#include "risip.h"

#include <QQmlEngine>
#include <QSettings>
#include <QCoreApplication>
#include <QDebug>
#include <QSortFilterProxyModel>

#include <QtQml>
#include <QQmlApplicationEngine>

#include "risipglobals.h"

#include "global_instances_factory.h"
#include "settings_json_storage_provider.h"
#include "accounts_settings.h"
#include "common_settings.h"
#include "codecs_settings.h"
#include "transport_pofiles_settings.h"
#include "risip_settings.h"
#include "conversion_helper.h"


#include "risipendpoint.h"
#include "risipbuddy.h"
#include "risipcall.h"
#include "risipaccountconfiguration.h"
#include "risipmedia.h"
#include "risipmessage.h"
#include "risipcallmanager.h"
#include "risipcontactmanager.h"
#include "risipphonecontact.h"
#include "risipphonenumber.h"
#include "risipratemanager.h"
#include "risip_types.h"


#include "utils/stopwatch.h"
#include "utils/qqmlsortfilterproxymodel.h"

#include "models/risipcallhistorymodel.h"
#include "models/risipcountryratesmodel.h"
#include "models/risipphonecontactsmodel.h"
#include "models/risipphonenumbersmodel.h"
#include "models/risipaccountlistmodel.h"
#include "models/risipmodels.h"

#include "apploader/risipapplicationsettings.h"


namespace risip {

class Risip::Private
{
public:
    RisipAccountListModel *accountsModel;
    RisipEndpoint sipEndpoint;
};

/**
 * @brief risipSingletonProvider
 * @param engine
 * @param scriptEngine
 * @return
 *
 * Risip singleton object provider for the QML engine.
 */
static QObject *risipSingletonProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    return Risip::instance();
}

/**
 * @brief risipCallManagerSingletonProvider
 * @param engine
 * @param scriptEngine
 * @return
 *
 * RisipCallManager singleton object provider for the QML engine.
 */
static QObject *risipCallManagerSingletonProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    return RisipCallManager::instance();
}

/**
 * @brief risipContactManagerSingletonProvider
 * @param engine
 * @param scriptEngine
 * @return
 *
 * RisipContactManager singleton object provider for the QML engine.
 */
static QObject *risipContactManagerSingletonProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    return RisipContactManager::instance();
}

static QObject *risipRateManagerSingletonProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    return RisipRateManager::instance();
}

static QObject *risipApplicationSingletonProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    return RisipApplicationSettings::instance();
}

Risip *Risip::m_risipInstance = NULL;
Risip *Risip::instance()
{
    if(!m_risipInstance)
        m_risipInstance = new Risip;

    return m_risipInstance;
}

Risip::Risip(QObject *parent)
    :QObject (parent)
    ,m_data(new Private)
{
    m_data->accountsModel = new RisipAccountListModel(this);
    RisipGlobals::instance()->initializeCountries();
}

Risip::~Risip()
{
    m_data->sipEndpoint.stop();
    m_data->accountsModel->deleteLater();
    delete m_data;
    m_data = nullptr;
}

RisipEndpoint *Risip::sipEndpoint() const
{
    return &m_data->sipEndpoint;
}

bool Risip::firstRun() const
{
    QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
    QVariant val = settings.value(RisipSettingsParam::FirstRun);
    if(val.isNull())
        return true;

    return settings.value(RisipSettingsParam::FirstRun).toBool();
}

QAbstractItemModel *Risip::allAccountsModel() const
{
    return m_data->accountsModel;
}

RisipAccount *Risip::activeAccount() const
{
    return m_data->accountsModel->activeAccount();
}

QString Risip::defaultAccount() const
{
    ISettingsProvider& settingsProvider  = GlobalInstancesFactory::settingsProvider();
    return QString::fromStdString(settingsProvider.settings()->commonSettings().m_defaultAccount);
}

void Risip::setDefaultAccount(const QString &uri)
{
    ISettingsProvider& settingsProvider  = GlobalInstancesFactory::settingsProvider();
    settingsProvider.settings()->commonSettings().m_defaultAccount = uri.toStdString();
    settingsProvider.write();

    emit defaultAccountChanged(uri);
}

void Risip::setActiveAccount(const QString &uri)
{
    m_data->accountsModel->setActiveAccountUri(uri);

    qDebug()<<"SIP ACTIVE ACCOUNT " << uri;
    emit activeAccountChanged(m_data->accountsModel->activeAccount());

    RisipCallManager::instance()->setActiveAccount(m_data->accountsModel->activeAccount());
    RisipContactManager::instance()->setActiveAccount(m_data->accountsModel->activeAccount());

    setDefaultAccount(uri);
}

void Risip::registerToQml()
{
    qmlRegisterSingletonType<Risip>(RisipSettingsParam::risipQmlURI, 1, 0, "Risip", risipSingletonProvider);
    qmlRegisterSingletonType<RisipCallManager>(RisipSettingsParam::risipQmlURI, 1, 0,
                                               "RisipCallManager", risipCallManagerSingletonProvider);
    qmlRegisterSingletonType<RisipContactManager>(RisipSettingsParam::risipQmlURI, 1, 0,
                                                  "RisipContactManager", risipContactManagerSingletonProvider);

    qmlRegisterSingletonType<RisipRateManager>(RisipSettingsParam::risipQmlURI, 1, 0,
                                               "RisipRateManager", risipRateManagerSingletonProvider);
    qmlRegisterSingletonType<RisipApplicationSettings>(RisipSettingsParam::risipQmlURI, 1, 0, "RisipApplicationSettings",
                                                       risipApplicationSingletonProvider);

    qmlRegisterType<RisipEndpoint>(RisipSettingsParam::risipQmlURI, 1, 0, "RisipEndpoint");
    qmlRegisterType<RisipAccount>(RisipSettingsParam::risipQmlURI, 1, 0, "RisipAccount");
    qmlRegisterType<RisipBuddy>(RisipSettingsParam::risipQmlURI, 1, 0, "RisipBuddy");
    qmlRegisterType<RisipCall>(RisipSettingsParam::risipQmlURI, 1, 0, "RisipCall");
    qmlRegisterType<RisipAccountConfiguration>(RisipSettingsParam::risipQmlURI, 1, 0, "RisipAccountConfiguration");
    qmlRegisterType<RisipMedia>(RisipSettingsParam::risipQmlURI, 1, 0, "RisipMedia");
    qmlRegisterType<RisipMessage>(RisipSettingsParam::risipQmlURI, 1, 0, "RisipMessage");
    qmlRegisterType<RisipCallHistoryModel>(RisipSettingsParam::risipQmlURI, 1, 0, "RisipCallHistoryModel");
    qmlRegisterType<RisipBuddiesModel>(RisipSettingsParam::risipQmlURI, 1, 0, "RisipBuddiesModel");
    qmlRegisterType<RisipContactHistoryModel>(RisipSettingsParam::risipQmlURI, 1, 0, "RisipContactHistoryModel");
    qmlRegisterType<RisipPhoneContactsModel>(RisipSettingsParam::risipQmlURI, 1, 0, "RisipPhoneContactsModel");
    qmlRegisterType<RisipPhoneContact>(RisipSettingsParam::risipQmlURI, 1, 0, "RisipPhoneContact");
    qmlRegisterType<RisipPhoneNumber>(RisipSettingsParam::risipQmlURI, 1, 0, "RisipPhoneNumber");
    qmlRegisterType<RisipPhoneNumbersModel>(RisipSettingsParam::risipQmlURI, 1, 0, "RisipPhoneNumbersModel");
    qmlRegisterType<RisipCountryRatesModel>(RisipSettingsParam::risipQmlURI, 1, 0, "RisipCountryRatesModel");
    qmlRegisterType<StopWatch>(RisipSettingsParam::risipQmlURI, 1, 0, "StopWatch");
    qmlRegisterType<qqsfpm::QQmlSortFilterProxyModel>(RisipSettingsParam::risipQmlURI, 1, 0, "SortFilterProxyModel");
}

RisipAccount *Risip::accountForUri(const QString &accountUri)
{
    return m_data->accountsModel->account(accountUri);
}


/**
 * @brief Risip::createNewAccount
 * @param username
 * @param password
 * @param serverAddress
 * @param proxyServer
 * @param localPort
 * @param networkProtocol
 * @return RisipAccount
 *
 * Creates a RisipAccount based on given params.
 */
RisipAccount *Risip::createNewAccount(const QString &username,
                                   const QString &password,
                                   const QString &serverAddress,
                                   const QString &proxyServer,
                                   int localPort,
                                   int networkProtocol)
{
    QString uri = ConversionHelperQ::createSipUri(username, serverAddress);
    std::string uriStdStr = uri.toStdString();

    pjsip_transport_type_e transportType = static_cast<pjsip_transport_type_e>(networkProtocol);

    auto settings = GlobalInstancesFactory::settingsProvider().settings();
    if (settings->accountsSettings().settings(uriStdStr)) {
        // already exists
        return nullptr;
    }

    TTransportProfileId trProfileId = std::string("TransportProfileId") + uriStdStr;
    AccountSettings accountSetting;
    accountSetting.m_accountConfig.idUri = uriStdStr;
    accountSetting.m_transportProfileId = trProfileId;
    pj::AuthCredInfo credInfo = {"digest", "*", username.toStdString(), 0, password.toStdString()};
    accountSetting.m_accountConfig.sipConfig.authCreds.push_back(credInfo);
    accountSetting.m_accountConfig.callConfig.timerMinSESec = 1200;
    accountSetting.m_accountConfig.callConfig.timerSessExpiresSec = 22000;
    accountSetting.m_accountConfig.regConfig.registrarUri = std::string("sip:") + serverAddress.toStdString();
    accountSetting.m_accountConfig.mediaConfig.srtpUse = PJMEDIA_SRTP_DISABLED;
    if (!proxyServer.isEmpty())
    {
        auto proxyUri = QString("sip:") + proxyServer + QString(";transport=");
        switch (transportType) {
        case PJSIP_TRANSPORT_UDP:
            proxyUri = proxyUri + QString("udp");
            break;
        case PJSIP_TRANSPORT_TCP:
            proxyUri = proxyUri + QString("tcp");
            break;
        case PJSIP_TRANSPORT_TLS:
            proxyUri = proxyUri + QString("tls");
            break;
//        case PJSIP_TRANSPORT_UDP6:
//            proxyUri = proxyUri + QString("udp6");
//            break;
//        case PJSIP_TRANSPORT_TCP6:
//            proxyUri = proxyUri + QString("tcp6");
//            break;
        default:
            break;
        }
        proxyUri = ConversionHelperQ::wrapToAngleBrackets(proxyUri);
        accountSetting.m_accountConfig.sipConfig.proxies = { proxyUri.toStdString() };
    }
    settings->accountsSettings().addSettings(uriStdStr, accountSetting);

    // TODO-S: every time transpport profile is created. is it right?
    TransportPofileSettings transpSettings;
    transpSettings.m_profileId = trProfileId;
    transpSettings.m_transportType = transportType;
    transpSettings.m_transportConfig.port = localPort;
    settings->transportProfileSettings().addSettings(transpSettings.m_profileId, transpSettings);
    GlobalInstancesFactory::settingsProvider().write();

    return createAccount(uri);
}

bool Risip::removeAccount(const QString &accountUri)
{
    auto settings = GlobalInstancesFactory::settingsProvider().settings();
    auto accountSettings =  settings->accountsSettings().settings(accountUri.toStdString());
    if (!accountSettings) {
        return false;
    }

    destroyAccount(accountUri);

    settings->transportProfileSettings().removeSettings(accountSettings->m_transportProfileId);
    settings->accountsSettings().removeSettings(accountUri.toStdString());
    GlobalInstancesFactory::settingsProvider().write();

    return true;
}


/**
 * @brief Risip::createAccount
 * @param uri
 * @return RisipAccount
 *
 * Creates a RisipAccount based on existing configuration of given SIP uri.
 */
RisipAccount *Risip::createAccount(const QString &uri)
{
    auto settings = GlobalInstancesFactory::settingsProvider().settings();
    auto accountSetting = settings->accountsSettings().settings(uri.toStdString());
    if (!accountSetting) {
        return nullptr;
    }
    auto transportSettings = settings->transportProfileSettings().settings(accountSetting->m_transportProfileId);
    if (!transportSettings) {
        return nullptr;
    }

    RisipAccount *account = new RisipAccount(m_data->accountsModel);
    account->setSipEndPoint(sipEndpoint());
    account->setAccountUri(uri);
    account->setAutoSignIn(accountSetting->m_autoSignIn);
    m_data->accountsModel->addSipAccount(account);
    RisipContactManager::instance()->createModelsForAccount(account);
    RisipCallManager::instance()->createModelsForAccount(account);

    return account;
}

bool Risip::destroyAccount(const QString &accountUri)
{
    if(m_data->accountsModel->exists(accountUri)) {
        // as parent of RisipAccount is RisipAccountListModel
        // then RisipAccount will be removed in RisipAccountListModel::removeSipAccount()
        m_data->accountsModel->removeSipAccount(accountUri);
        RisipContactManager::instance()->removeModelsForAccount(accountForUri(accountUri));
        RisipCallManager::instance()->removeModelsForAccount(accountForUri(accountUri));

        return true;
    }

    return false;
}


bool Risip::saveSettings()
{
    ISettingsProvider& settingsProvider  = GlobalInstancesFactory::settingsProvider();
    if (!settingsProvider.write()) {
        return false;
    }
    return true;
}

bool Risip::start()
{
    ISettingsProvider& settingsProvider  = GlobalInstancesFactory::settingsProvider();
    if (!settingsProvider.read()) {
        // settings are corrupted - create initial settings
        if (!createInitialSettings())
            return false;
    }

    if (sipEndpoint()->start() != RisipEndpoint::Started) {
        return false;
    }

    return createAccounts();
}

bool Risip::stop()
{
    // TODO-S: don't sure than we need to save settings when stop
    ISettingsProvider& settingsProvider  = GlobalInstancesFactory::settingsProvider();
    if (!settingsProvider.write()) {
        return false;
    }

    destroyAccounts();

    return sipEndpoint()->stop();
}

/**
 * @brief Risip::login
 * @param accountUri
 * @param passw: password, ignored if account's m_autoSignIn is true
 * @return
 */
bool Risip::login(const QString &accountUri, const QString &passw)
{
    // logout previous account
    activeAccount()->logout();

    setActiveAccount(accountUri);
    RisipAccount* account = activeAccount();
    return account->login(passw);
}

bool Risip::resetSettings()
{
    stop();
    createInitialSettings();
    return start();
}

bool Risip::createAccounts()
{
    ISettingsProvider& settingsProvider  = GlobalInstancesFactory::settingsProvider();
    for(auto accountUrl: settingsProvider.settings()->accountsSettings().accounts()) {
        RisipAccount *account = createAccount(QString::fromStdString(accountUrl));
        if (!account) {
            // TODO-S: log about issue
        }
    }

    TAccountUri defaultAccountUri = settingsProvider.settings()->commonSettings().m_defaultAccount;
    if (defaultAccountUri.empty()) {
        return true;
    }

    setDefaultAccount(QString::fromStdString(defaultAccountUri));
    return true;
}

bool Risip::destroyAccounts()
{
    ISettingsProvider& settingsProvider  = GlobalInstancesFactory::settingsProvider();
    for(auto accountUrl: settingsProvider.settings()->accountsSettings().accounts()) {
        destroyAccount(QString::fromStdString(accountUrl));
    }

    return false;
}

bool Risip::createInitialSettings()
{
    auto settings  = GlobalInstancesFactory::settingsProvider().settings();
    settings->clear();

    CommonSettings &commonSettings = settings->commonSettings();
    commonSettings.m_defaultAccount = "";
    commonSettings.m_EndpointConfig.logConfig.level = 5;
    commonSettings.m_EndpointConfig.uaConfig.maxCalls = 4;
    commonSettings.m_EndpointConfig.medConfig.sndClockRate = 16000;

    CodecSettings codecSettings;
    codecSettings.m_codecId = "PCMA/8000";
    codecSettings.m_codecPriority = 255;
    codecSettings.m_useDefaultParams = false;
    
    codecSettings.m_codecParams.info.clockRate = 8000;
    codecSettings.m_codecParams.info.channelCnt = 1;
    codecSettings.m_codecParams.info.avgBps = 64000;
    codecSettings.m_codecParams.info.maxBps = 64000;
    codecSettings.m_codecParams.info.maxRxFrameSize = 80;
    codecSettings.m_codecParams.info.frameLen = 10;
    codecSettings.m_codecParams.info.pcmBitsPerSample = 16;
    codecSettings.m_codecParams.info.fmtId = PJMEDIA_FORMAT_L16;

    codecSettings.m_codecParams.info.pt = PJMEDIA_RTP_PT_PCMA;

    codecSettings.m_codecParams.setting.plc = true;
    codecSettings.m_codecParams.setting.vad = true;
    codecSettings.m_codecParams.setting.cng = false;
    codecSettings.m_codecParams.setting.frmPerPkt = 2;
    codecSettings.m_codecParams.setting.penh = false;

    settings->codecsSettings().addSettings(codecSettings.m_codecId, codecSettings);

    return GlobalInstancesFactory::settingsProvider().write();
}



void Risip::accessPhoneContacts()
{
    RisipContactManager::instance()->fetchPhoneContacts();
}

void Risip::accessPhoneMedia()
{
    RisipMedia media;
    media.loopAudioTest();
}

void Risip::accessPhoneLocation()
{
}

} //end of risip namespace
