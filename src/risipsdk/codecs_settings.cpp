#include "codecs_settings.h"



CodecsSettings::CodecsSettings()
{
}

CodecsSettings::~CodecsSettings()
{
}

const TCodecIdVector &CodecsSettings::codecs() const
{
    if (m_codecsListModified) {
        m_codecs.clear();
        m_codecs.reserve(m_codecSettingsMap.size());
        for(auto kv : m_codecSettingsMap) {
            m_codecs.push_back(kv.first);
        }
        m_codecsListModified = false;
    }
    return m_codecs;
}

const CodecSettings *CodecsSettings::settings(const TCodecId &codecId) const
{
    if (m_codecSettingsMap.find(codecId) != m_codecSettingsMap.end())
    {
        return &m_codecSettingsMap.at(codecId);
    }
    return nullptr;
}

CodecSettings *CodecsSettings::settings(const TCodecId &codecId)
{
    if (m_codecSettingsMap.find(codecId) != m_codecSettingsMap.end())
    {
        return &m_codecSettingsMap.at(codecId);
    }
    return nullptr;
}

void CodecsSettings::addSettings(const TCodecId &codecId, const CodecSettings &settings)
{
    m_codecSettingsMap[codecId] = settings;
    m_codecsListModified = true;
}

//void CodecsSettings::addCodecSettings(const string &TCodecId, const CodecSettings &&settings)
//{
//    m_codecSettingsMap[codecId] = settings;
//    m_accountsListModified = true;
//}

void CodecsSettings::removeSettings(const TCodecId &codecId)
{
    if (m_codecSettingsMap.find(codecId) != m_codecSettingsMap.end())
    {
        m_codecSettingsMap.erase(codecId);
        m_codecsListModified = true;
    }
}
