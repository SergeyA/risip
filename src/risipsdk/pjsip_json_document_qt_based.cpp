#ifndef PJSIP_JSON_DOCUMENT_QT_BASED_CPP
#define PJSIP_JSON_DOCUMENT_QT_BASED_CPP


#include "pjsip_json_document_qt_based.h"

#include <string>
#include <vector>

#include <pjsua2/persistent.hpp>
#include <qjsondocument.h>
#include <qjsonobject.h>
#include <qjsonarray.h>
#include <qjsonvalue.h>
#include <qfile.h>

#include "risipglobals.h"

using namespace std;
using namespace pj;



static bool          jsonqtNode_hasUnread(const ContainerNode*);
static string        jsonqtNode_unreadName(const ContainerNode*n) throw(Error);
static float         jsonqtNode_readNumber(const ContainerNode*, const string&) throw(Error);
static bool          jsonqtNode_readBool(const ContainerNode*, const string&) throw(Error);
static string        jsonqtNode_readString(const ContainerNode*, const string&) throw(Error);
static StringVector  jsonqtNode_readStringVector(const ContainerNode*, const string&) throw(Error);
static ContainerNode jsonqtNode_readContainer(const ContainerNode*, const string &) throw(Error);
static ContainerNode jsonqtNode_readArray(const ContainerNode*, const string &) throw(Error);
static void          jsonqtNode_writeNumber(ContainerNode*, const string &name, float num) throw(Error);
static void          jsonqtNode_writeBool(ContainerNode*, const string &name, bool value) throw(Error);
static void          jsonqtNode_writeString(ContainerNode*, const string &name, const string &value) throw(Error);
static void          jsonqtNode_writeStringVector(ContainerNode*, const string &name, const StringVector &value) throw(Error);
static ContainerNode jsonqtNode_writeNewContainer(ContainerNode*, const string &name) throw(Error);
static ContainerNode jsonqtNode_writeNewArray(ContainerNode*, const string &name) throw(Error);

static container_node_op jsonqt_op = {
    &jsonqtNode_hasUnread,
    &jsonqtNode_unreadName,
    &jsonqtNode_readNumber,
    &jsonqtNode_readBool,
    &jsonqtNode_readString,
    &jsonqtNode_readStringVector,
    &jsonqtNode_readContainer,
    &jsonqtNode_readArray,
    &jsonqtNode_writeNumber,
    &jsonqtNode_writeBool,
    &jsonqtNode_writeString,
    &jsonqtNode_writeStringVector,
    &jsonqtNode_writeNewContainer,
    &jsonqtNode_writeNewArray
};


using TQJsonValuePtrVector = std::vector<QJsonValue*>;

class PjsipJsonDocumentQtBasedReaderOnly::Private
{
public:
    Private(){}
    mutable pj::ContainerNode _rootNode;
    QJsonDocument _document;
    TQJsonValuePtrVector _qJsonValuePtrVector;
};

PjsipJsonDocumentQtBasedReaderOnly::PjsipJsonDocumentQtBasedReaderOnly()
    : m_data(new PjsipJsonDocumentQtBasedReaderOnly::Private())
{
    initRoot();
}

void PjsipJsonDocumentQtBasedReaderOnly::addPointer(void *pval)
{
    if (pval == nullptr) {
        return;
    }
    QJsonValue* pjval = static_cast<QJsonValue*>(pval);
    m_data->_qJsonValuePtrVector.push_back(pjval);
}

void PjsipJsonDocumentQtBasedReaderOnly::clearPointers()
{
    for(int i = 0; i<m_data->_qJsonValuePtrVector.size(); i++) {
        delete m_data->_qJsonValuePtrVector[i];
    }
    m_data->_qJsonValuePtrVector.clear();
}

PjsipJsonDocumentQtBasedReaderOnly::~PjsipJsonDocumentQtBasedReaderOnly()
{
    clearPointers();
    delete m_data;
}

void PjsipJsonDocumentQtBasedReaderOnly::initRoot()
{
    m_data->_rootNode.op = &jsonqt_op;
    m_data->_rootNode.data.doc = this;
    if (m_data->_document.isObject()) {
        QJsonValue* pval = new QJsonValue(m_data->_document.object());
        m_data->_rootNode.data.data1 = pval;
        this->addPointer(pval);
    }
    else if (m_data->_document.isArray()) {
        QJsonValue* pval = new QJsonValue(m_data->_document.array());
        m_data->_rootNode.data.data1 = pval;
        this->addPointer(pval);
    }
    else {
        m_data->_rootNode.data.data1 = nullptr;
    }

}

void PjsipJsonDocumentQtBasedReaderOnly::loadFile(const std::string &filename) throw(pj::Error)
{
    // clean document
    m_data->_document = QJsonDocument();

    QFile loadFile(QString::fromStdString(filename));
    if (!loadFile.open(QIODevice::ReadOnly)) {
        throw Error(1, "Could not open file", filename, __FILE__, __LINE__);
    }

    QByteArray saveData = loadFile.readAll();
    m_data->_document = QJsonDocument::fromJson(saveData);
    if (m_data->_document.isNull()) {
        throw Error(1, "Error of loading JSON file", filename, __FILE__, __LINE__);
    }
    initRoot();
}

void PjsipJsonDocumentQtBasedReaderOnly::loadString(const std::string &input) throw(pj::Error)
{
    // clean document
    m_data->_document = QJsonDocument();

    m_data->_document = QJsonDocument::fromJson(input.c_str());
    if (m_data->_document.isNull()) {
        throw Error(1, "Error of loading JSON string", input, __FILE__, __LINE__);
    }
    initRoot();
}

void PjsipJsonDocumentQtBasedReaderOnly::saveFile(const std::string &filename) throw(pj::Error)
{
    throw Error(1, "Not implementd", filename, __FILE__, __LINE__);
}

std::string PjsipJsonDocumentQtBasedReaderOnly::saveString() throw(pj::Error)
{
    throw Error(1, "Not implementd", "", __FILE__, __LINE__);
    return "";
}

pj::ContainerNode &PjsipJsonDocumentQtBasedReaderOnly::getRootContainer() const
{
    return m_data->_rootNode;
}

static bool          jsonqtNode_hasUnread(const ContainerNode *node)
{
    QJsonValue* aval = static_cast<QJsonValue*>(node->data.data1);
    if (aval->isArray()) {
        QJsonArray array = aval->toArray();
        const int index = reinterpret_cast<const size_t>(node->data.data2);
        if (index >= array.size()) {
            return false;
        }
        return true;
    }
    return false;
}

static std::string        jsonqtNode_unreadName(const ContainerNode *node) throw(Error)
{
    // we don't support this function
    return "";
}

static QJsonValue getValue(const ContainerNode *node, const string &name) throw(Error)
{
    QJsonValue* aval = static_cast<QJsonValue*>(node->data.data1);
    QJsonValue val;
    if (aval->isArray()) {
        QJsonArray array = aval->toArray();
        const int *cind = reinterpret_cast<const int*>(&node->data.data2);
        int* pindex = const_cast<int*>(cind);
        if ( *pindex >= array.size()) {
            throw Error(1, "Array doesn't have more values",  "", __FILE__, __LINE__);
        }
        val = array[*pindex];
        (*pindex)++;
    }
    else if (aval->isObject()) {
        QJsonObject obj = aval->toObject();
        val = obj.value(QString::fromStdString(name));
    }
    else {
        throw Error(1, "Wrong value type",  name, __FILE__, __LINE__);
    }
    return val;
}

static float         jsonqtNode_readNumber(const ContainerNode *node, const string &name) throw(Error)
{
    QJsonValue val = getValue(node, name);

    if (val.type() == QJsonValue::Undefined) {
        throw Error(1, "Value is not exist",  name, __FILE__, __LINE__);
    }
    if (val.type() != QJsonValue::Double) {
        throw Error(1, "Value is not 'Double'",  name, __FILE__, __LINE__);
    }
    return val.toDouble();

}

static bool          jsonqtNode_readBool(const ContainerNode *node, const string &name) throw(Error)
{
    QJsonValue val = getValue(node, name);

    if (val.type() == QJsonValue::Undefined) {
        throw Error(1, "Value is not exist",  name, __FILE__, __LINE__);
    }
    if (val.type() != QJsonValue::Bool) {
        throw Error(1, "Value is not 'Bool'",  name, __FILE__, __LINE__);
    }
    return val.toBool();


}

static string        jsonqtNode_readString(const ContainerNode *node, const string &name) throw(Error)
{
    QJsonValue val = getValue(node, name);

    if (val.type() == QJsonValue::Undefined) {
        throw Error(1, "Value is not exist",  name, __FILE__, __LINE__);
    }
    if (val.type() != QJsonValue::String) {
        throw Error(1, "Value is not 'String'",  name, __FILE__, __LINE__);
    }
    return val.toString().toStdString();
}

static StringVector  jsonqtNode_readStringVector(const ContainerNode *node, const string &name) throw(Error)
{
    QJsonValue val = getValue(node, name);

    if (val.type() == QJsonValue::Undefined) {
        throw Error(1, "Value is not exist",  name, __FILE__, __LINE__);
    }
    if (val.type() != QJsonValue::Array) {
        throw Error(1, "Value is not 'Vector'",  name, __FILE__, __LINE__);
    }
    QJsonArray array = val.toArray();
    StringVector result;
    for (int i = 0; i < array.size(); ++i)
    {
        result.push_back(array[i].toString().toStdString());
    }
    return result;
}

static ContainerNode jsonqtNode_readContainer(const ContainerNode *node, const string &name) throw(Error)
{
    QJsonValue val = getValue(node, name);
    if (val.type() == QJsonValue::Undefined) {
        throw Error(1, "Value is not exist",  name, __FILE__, __LINE__);
    }
    if (val.type() != QJsonValue::Object) {
        throw Error(1, "Value is not 'Container'",  name, __FILE__, __LINE__);
    }

    ContainerNode childNode;
    childNode.op = &jsonqt_op;
    childNode.data.doc = node->data.doc;
    QJsonValue* pval = new QJsonValue(val);
    PjsipJsonDocumentQtBasedReaderOnly* doc = static_cast<PjsipJsonDocumentQtBasedReaderOnly*>(node->data.doc);
    doc->addPointer(pval);
    childNode.data.data1 = pval;

    return childNode;
}

static ContainerNode jsonqtNode_readArray(const ContainerNode *node, const string &name) throw(Error)
{
    QJsonValue val = getValue(node, name);
    if (val.type() == QJsonValue::Undefined) {
        throw Error(1, "Value is not exist",  name, __FILE__, __LINE__);
    }
    if (val.type() != QJsonValue::Array) {
        throw Error(1, "Value is not 'Container'",  name, __FILE__, __LINE__);
    }

    ContainerNode childNode;
    childNode.op = &jsonqt_op;
    childNode.data.doc = node->data.doc;
    QJsonValue* pval = new QJsonValue(val);
    PjsipJsonDocumentQtBasedReaderOnly* doc = static_cast<PjsipJsonDocumentQtBasedReaderOnly*>(node->data.doc);
    doc->addPointer(pval);
    childNode.data.data1 = pval;
    childNode.data.data2 = 0;

    return childNode;
}

static void          jsonqtNode_writeNumber(ContainerNode *node, const string &name, float num) throw(Error)
{
    throw Error(1, "Not implementd", "", __FILE__, __LINE__);
}

static void          jsonqtNode_writeBool(ContainerNode *node, const string &name, bool value) throw(Error)
{
    throw Error(1, "Not implementd", "", __FILE__, __LINE__);
}

static void          jsonqtNode_writeString(ContainerNode *node, const string &name, const string &value) throw(Error)
{
    throw Error(1, "Not implementd", "", __FILE__, __LINE__);
}

static void          jsonqtNode_writeStringVector(ContainerNode *node, const string &name, const StringVector &value) throw(Error)
{
    throw Error(1, "Not implementd", "", __FILE__, __LINE__);
}

static ContainerNode jsonqtNode_writeNewContainer(ContainerNode *node, const string &name) throw(Error)
{
    throw Error(1, "Not implementd", "", __FILE__, __LINE__);
}

static ContainerNode jsonqtNode_writeNewArray(ContainerNode *node, const string &name) throw(Error)
{
    throw Error(1, "Not implementd", "", __FILE__, __LINE__);
}



#endif // PJSIP_JSON_DOCUMENT_QT_BASED_CPP
