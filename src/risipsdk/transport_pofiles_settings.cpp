#include "transport_pofiles_settings.h"

TransportPofilesSettings::TransportPofilesSettings()
{

}

TransportPofilesSettings::~TransportPofilesSettings()
{

}

const TTransportProfileIdVector &TransportPofilesSettings::transportProfiles() const
{
    if (m_transportProfilesListModified) {
        m_transportProfiles.clear();
        m_transportProfiles.reserve(m_transportPofileSettingsMap.size());
        for(auto kv : m_transportPofileSettingsMap) {
            m_transportProfiles.push_back(kv.first);
        }
        m_transportProfilesListModified = false;
    }
    return m_transportProfiles;
}

const TransportPofileSettings *TransportPofilesSettings::settings(const TTransportProfileId &profileId) const
{
    if (m_transportPofileSettingsMap.find(profileId) != m_transportPofileSettingsMap.end())
    {
        return &m_transportPofileSettingsMap.at(profileId);
    }
    return nullptr;

}

TransportPofileSettings *TransportPofilesSettings::settings(const TTransportProfileId &profileId)
{
    if (m_transportPofileSettingsMap.find(profileId) != m_transportPofileSettingsMap.end())
    {
        return &m_transportPofileSettingsMap.at(profileId);
    }
    return nullptr;
}

void TransportPofilesSettings::addSettings(const TTransportProfileId &profileId, const TransportPofileSettings &settings)
{
    m_transportPofileSettingsMap[profileId] = settings;
    m_transportProfilesListModified = true;
}

void TransportPofilesSettings::removeSettings(const TTransportProfileId &profileId)
{
    if (m_transportPofileSettingsMap.find(profileId) != m_transportPofileSettingsMap.end())
    {
        m_transportPofileSettingsMap.erase(profileId);
        m_transportProfilesListModified = true;
    }
}
