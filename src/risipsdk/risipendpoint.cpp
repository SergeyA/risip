/***********************************************************************************
**    Copyright (C) 2016  Petref Saraci
**    http://risip.io
**
**    This program is free software: you can redistribute it and/or modify
**    it under the terms of the GNU General Public License as published by
**    the Free Software Foundation, either version 3 of the License, or
**    (at your option) any later version.
**
**    This program is distributed in the hope that it will be useful,
**    but WITHOUT ANY WARRANTY; without even the implied warranty of
**    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**    GNU General Public License for more details.
**
**    You have received a copy of the GNU General Public License
**    along with this program. See LICENSE.GPLv3
**    A copy of the license can be found also here <http://www.gnu.org/licenses/>.
**
************************************************************************************/

#include "risipendpoint.h"

#include <unordered_map>

#include "risipaccountconfiguration.h"
#include "risip.h"
#include "risip_types.h"
#include "global_instances_factory.h"
#include "settings_provider.h"
#include "risip_settings.h"
#include "common_settings.h"
#include "codecs_settings.h"
#include "transport_pofiles_settings.h"

#include "pjsipwrapper/pjsipendpoint.h"

#include <QDebug>

namespace risip {

struct TransportIdAndCounter
{
    TransportId m_transportId;
    int m_refCounter;
};

using TTransportProfileIdMap = std::unordered_map<TTransportProfileId, TransportIdAndCounter>;

class RisipEndpoint::Private
{
public:
    PjsipEndpoint *pjsipEndpoint;
    TTransportProfileIdMap transportProfileIdMap;
    Error error;
};

RisipEndpoint::RisipEndpoint(QObject *parent)
    :QObject(parent)
    ,m_data(new Private)
{
}

RisipEndpoint::~RisipEndpoint()
{
    delete PjsipEndpoint::instance();
    m_data->pjsipEndpoint = NULL;

    delete m_data;
    m_data = NULL;
}

/**
 * @brief RisipEndpoint::status
 * @return status of the SIP endpoint / engine
 *
 * Use this property to see the status of the SIP library whether it has started, stoped or
 * it has an error.
 */
int RisipEndpoint::status() const
{
    if(!m_data->pjsipEndpoint)
        return RisipEndpoint::NotStarted;

    switch (m_data->pjsipEndpoint->libGetState()) {
    case PJSUA_STATE_NULL:
    case PJSUA_STATE_CREATED:
    case PJSUA_STATE_INIT:
    case PJSUA_STATE_STARTING:
    case PJSUA_STATE_CLOSING:
        return RisipEndpoint::NotStarted;
    case PJSUA_STATE_RUNNING:
        return RisipEndpoint::Started;
    default:
        return RisipEndpoint::EngineError;
    }
}

QQmlListProperty<TTransportProfileId> RisipEndpoint::activeTransportProfileIdList() const
{
    // TODO-S: implement it
    QQmlListProperty<TTransportProfileId> idList;
    return idList;
}

/**
 * @brief RisipEndpoint::errorCode
 * @return the last error code if the status of SIP endpoint is EngineError
 *
 * If the status of this endpoint is EngineError @see RisipEndpoint::status then use this property
 * to errorCode get the last error code.
 */
int RisipEndpoint::errorCode() const
{
    return m_data->error.status;
}

/**
 * @brief RisipEndpoint::errorMessage
 * @return returns the last error message
 *
 * Use this property to retrieve the last error message.
 */
QString RisipEndpoint::errorMessage() const
{
    return QString::fromStdString(m_data->error.reason);
}

/**
 * @brief RisipEndpoint::errorInfo
 * @return returns the last error complete info message
 *
 * Use this property to see the complete error information
 */
QString RisipEndpoint::errorInfo() const
{
    return QString::fromStdString(m_data->error.info(true));
}

TransportId RisipEndpoint::createTransport(const TTransportProfileId& transportProfileId)
{   
    if(status() == NotStarted
            || status() == EngineError)
        return -1;

    auto transportIt = m_data->transportProfileIdMap.find(transportProfileId);
    if (transportIt != m_data->transportProfileIdMap.end()) {
        transportIt->second.m_refCounter++;
        return transportIt->second.m_transportId;
    }

    auto settings = GlobalInstancesFactory::settingsProvider().settings();
    auto transportSettings =  settings->transportProfileSettings().settings(transportProfileId);
    if (!transportSettings) {
        return -1;
    }

    TransportId transportId = -1;
    try {
        transportId = m_data->pjsipEndpoint->transportCreate(transportSettings->m_transportType, transportSettings->m_transportConfig);
        m_data->transportProfileIdMap[transportProfileId] = {transportId, 1};
    } catch (Error& err) {
        setError(err);
        return -1;
    }

    return transportId;
}



/**
 * @brief RisipEndpoint::destroyActiveTransport
 * @return true/false if transport is destoryed or not.
 *
 * Internal API.
 */
bool RisipEndpoint::destroyTransport(const TTransportProfileId &transportProfileId)
{
    auto transportIt = m_data->transportProfileIdMap.find(transportProfileId);
    if (transportIt == m_data->transportProfileIdMap.end()) {
        return false;
    }

    transportIt->second.m_refCounter--;
    if (transportIt->second.m_refCounter > 0) {
        return true;
    }

    try {
        m_data->pjsipEndpoint->transportClose(transportIt->second.m_transportId);
    } catch(Error& err) {
        setError(err);
        return false;
    }

    return true;
}

PjsipEndpoint *RisipEndpoint::endpointInstance()
{
    return PjsipEndpoint::instance();
}

/**
 * @brief RisipEndpoint::start
 *
 * Use this function to start the SIP endpoint library/engine.
 * MUST call this before any other operation with Risip objects.
 */
int RisipEndpoint::start()
{
    m_data->pjsipEndpoint = PjsipEndpoint::instance();
    m_data->pjsipEndpoint->setRisipEndpointInterface(this);


    try {
        m_data->pjsipEndpoint->libCreate();
    } catch (Error &err) {
        emit statusChanged(status());
        setError(err);
        return status();
    }

    auto settings = GlobalInstancesFactory::settingsProvider().settings();
    try {
        m_data->pjsipEndpoint->libInit(settings->commonSettings().m_EndpointConfig);
    } catch (Error &err) {
        emit statusChanged(status());
        setError(err);
        return status();
    }

    try {
        m_data->pjsipEndpoint->libStart();
    } catch (Error &err) {
        emit statusChanged(status());
        setError(err);
        return status();
    }

    for(auto codecId: settings->codecsSettings().codecs()) {
        auto sett = settings->codecsSettings().settings(codecId);
        Endpoint::instance().codecSetPriority(sett->m_codecId, sett->m_codecPriority);
        if (!sett->m_useDefaultParams) {
            Endpoint::instance().codecSetParam(sett->m_codecId, sett->m_codecParams);
        }
    }


    CodecInfoVector codecs = Endpoint::instance().codecEnum();
    CodecInfo *codecInfo;
    for(int i=0; i<codecs.size(); ++i) {
        codecInfo = codecs.at(i);
        qDebug()<<"CODEC INFO: " << QString::fromStdString(codecInfo->codecId)
                << QString::fromStdString(codecInfo->desc)
                << codecInfo->priority;

//        CodecParam param = Endpoint::instance().codecGetParam(codecInfo->codecId);
//        int jj = 0;
    }

    emit statusChanged(status());
    return status();
}

/**
 * @brief RisipEndpoint::stop
 *
 * Call this function to stop the SIP endpoint / library
 */
int RisipEndpoint::stop()
{
    if(m_data->pjsipEndpoint && m_data->pjsipEndpoint->libGetState() != PJSUA_STATE_NULL)
        m_data->pjsipEndpoint->libDestroy();

    emit statusChanged(status());

    return status();
}

void RisipEndpoint::setError(const Error &error)
{
    qDebug()<<"ERROR: " <<"code: "<<error.status <<" info: " << QString::fromStdString(error.info(true));

    if(m_data->error.status != error.status) {

        m_data->error.status = error.status;
        m_data->error.reason = error.reason;
        m_data->error.srcFile = error.srcFile;
        m_data->error.srcLine = error.srcLine;
        m_data->error.title = error.title;

        emit errorCodeChanged(m_data->error.status);
        emit errorMessageChanged(QString::fromStdString(m_data->error.reason));
        emit errorInfoChanged(QString::fromStdString(m_data->error.info(true)));
    }
}

} //end of risip namespace
